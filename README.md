# Generate report

## GET /report?asDate=2021-12-31&status=SUCCESS,FAILED

#### Request headers (optional)

```
* Content-Type: "application/json"
```

#### Request parameters (optional)

```
* asDate - required, transaction date format yyyy-mm-dd  eg. 2021-12-31
* status - optional (return only success if not specific) status of transaction support multiple value (SUCCESS,FAILED)
```

#### Response data

##### if found data

```
http status : 200

data file (in Binary)
```

##### in case no data found return
```
http status : 404

json body
{
  "code" : "10001",
  "description" : "Not found"
}
```

##### not input asDate
```
http status : 400

json body
{
  "code" : "10000",
  "description" : "asDate is invalid"
}
```


#### Query

```sql
SELECT
date_format(l.updated_at, '%Y-%m-%d') AS change_date,
loan_account_number AS lending_account_number,
product_code AS product,
old_value AS credit_line_old,
new_value AS credit_line_new,
description_en AS correction_type,
updated_by AS done_by_user
FROM crarr.limit_update_log l
JOIN crarr.change_codes c ON l.correction_type = c.id
WHERE l.updated_at >= {asOfDate} AND l.updated_at < {asOfDate + 1}
AND status IN {statusList}
```


### System Flow for API

```plantuml
participant "User" as usr
participant "API" as api
participant "DB" as db

usr -> api : GET /report?asDate=2021-12-31&status=SUCCESS,FAILED
api -> db : inquiry data by criteria
api <-- db : data

alt found data
api -> api : generate data file (csv format)
usr <-- api : Response report in binary data
else no data found
usr <-- api : response 404 { "code" : "10001", "description" : "Not found" }
end

```
