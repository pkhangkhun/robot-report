*** Settings ***
Resource         ../../resources/imports.robot

*** Keywords ***
Call Get data file from service
    [Arguments]     ${expected_status}=200
    create session    getDataFile    ${service_url}
    ${headers}=     Create Dictionary    X-ACCESS-KEY='robot-test'
    ${response}=    Get On Session    getDataFile    ${report_api}/result.csv    headers=${headers}    expected_status=${expected_status}
    set test variable    ${response}
