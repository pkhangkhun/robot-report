*** Settings ***
Resource    ../../resources/imports.robot

*** Keywords ***
Generate Headers
    [Arguments]         ${access_key}
    ${correlation_id}    common_util.generate_uuid    ${application.name}
    ${headers}    create dictionary    Content-Type=application/json
    ...  X-ACCESS-KEY=${access_key}    X-Correlation-ID=${correlation_id}
    [Return]    ${headers}

HTTP status should be '${expected_http}'
    [Documentation]    Verify response http status with expected code
    should be equal as integers    ${response.status_code}    ${expected_http}
