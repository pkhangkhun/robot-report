*** Settings ***
Resource        ../resources/imports.robot

*** Keywords ***
Connect To Test Database
    Connect To Database   pymysql    ${db_name}    ${db_user}    ${db_pwd}    ${db_host}    ${db_port}

Disconnect from Test Database
    Disconnect From Database

Execute Database script
    [Arguments]     ${resource_sql_path}
    ${sql_insert}=    Execute Sql Script    ${resource_sql_path}

Verify status should be
    [Arguments]     ${id}       ${status}
    ${output} =    Query    SELECT status FROM limit_update_log WHERE id = ${id};
    Should Be Equal As Strings    ${output[0][0]}    ${status}      ## first row, first column value
