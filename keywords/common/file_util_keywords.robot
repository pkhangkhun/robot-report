*** Settings ***
Resource    ../../resources/imports.robot

*** Keywords ***
Save context to local file
    [Arguments]     ${content}    ${local_path}
    Create Binary File     ${local_path}   ${content}

Compare 2 files on localPath
    [Arguments]     ${source_file_path}     ${target_file_path}    ${expected_encoding}=UTF-8
    ${source} =    OperatingSystem.Get File    ${source_file_path}      encoding=${expected_encoding}
    ${target} =    OperatingSystem.Get File    ${target_file_path}      encoding=${expected_encoding}
    Should Be Equal As Strings      ${source}   ${target}
