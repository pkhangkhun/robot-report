*** Settings ***
Library         RequestsLibrary

Variables       common_configs.yaml
Variables       configs/${ENV}/env_config.yaml

Resource        ../keywords/common/common_keywords.robot

Library         Collections
Library         String
Library         OperatingSystem
Library         JSONLibrary
Library         DatabaseLibrary
