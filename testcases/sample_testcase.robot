*** Settings ***
Resource     ../resources/imports.robot
Resource     ../keywords/api/report_api_keywords.robot
Resource     ../keywords/common/db_keywords.robot
Resource     ../keywords/common/file_util_keywords.robot

Suite Setup         Run Keywords    Connect To Test Database
                    ...      AND    Execute database script     ${CURDIR}/../resources/sql/create_data.sql

Suite Teardown      Run Keywords    Execute database script     ${CURDIR}/../resources/sql/delete_data.sql
                    ...      AND    Disconnect from Test Database


*** Test Cases ***
Sample - Connect to db
    Verify status should be     id=100      status=SUCCESS

Sample - Get data file from service
    When Call Get data file from service
    Then HTTP status should be '200'
    And Save context to local file      content=${response.content}    local_path=tmp/file1.csv
    And Compare 2 files on localPath    source_file_path=tmp/file1.csv     target_file_path=../resources/testdata/${EVN}/output_tc_01.csv
